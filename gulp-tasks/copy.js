'use strict';

var gulp = require( 'gulp' );


// BROWSER SYNC ------------------------------------------

gulp.task( 'copy:html', function() {
  gulp.src( "src/*.html" )
    .pipe( gulp.dest( "./" ) );
});