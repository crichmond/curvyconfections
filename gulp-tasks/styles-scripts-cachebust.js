'use strict';

var gulp = require( 'gulp' );
var runSequence = require( 'run-sequence' );
var clean = require( 'gulp-clean' );
var CacheBuster = require( 'gulp-cachebust' );
var cachebust = new CacheBuster();


// STYLES --------------------------------------------

var sourcemaps = require( "gulp-sourcemaps" ),
  cssmin = require( 'gulp-cssmin' ),
  sass = require( "gulp-sass" ),
  compileSASS = function( _source, _dest, _productionReady ) {
    if( _productionReady ) {
      return gulp.src( _source )
        .pipe( sass( {
          errLogToConsole: true
        }) )
        .pipe( cssmin() )
        .pipe( gulp.dest( _dest ) );
    } else {
      return gulp.src( _source )
        .pipe( sourcemaps.init() )
        .pipe( sass( {
          errLogToConsole: false,
          includePaths: [
            'bower_components/bourbon/app/assets/stylesheets'
          ]
        }) )
        .pipe( sourcemaps.write() )
        .pipe( gulp.dest( _dest ) );
    }
  };

gulp.task( "styles:clean", function() {
  return gulp.src( [
    'dist/styles'
  ], {
      read: false
    })
    .pipe( clean( {
      force: true
    }) );
});


gulp.task( 'styles:cachebust', function() {
  return gulp.src( 'dist/styles/*.css' )
    .pipe( cachebust.resources() )
    .pipe( gulp.dest( 'dist/styles/' ) );
});

gulp.task( "styles:compile", [ "styles:clean" ], function() {
  return compileSASS( [ "src/styles/site.scss" ], "dist/styles/", false );
});

gulp.task( "styles", function( cb ) {
  return runSequence( "styles:compile", "styles:cachebust", cb );
});


// SCRIPTS --------------------------------------------


gulp.task( "scripts:clean", function() {
  return gulp.src( [
    'dist/scripts'
  ], {
      read: false
    })
    .pipe( clean( {
      force: true
    }) );
});

gulp.task( "scripts:compile", [ "scripts:clean" ], function( cb ) {
  // eventually something will be here
  cb();
});

gulp.task( 'scripts:cachebust', function() {
  return gulp.src( 'dist/scripts/*.js' )
    .pipe( cachebust.resources() )
    .pipe( gulp.dest( 'dist/scripts/' ) );
});

gulp.task( "scripts", function( cb ) {
  return runSequence( "scripts:compile", "scripts:cachebust", cb );
});



// HTML INJECTING --------------------------------------------------------


gulp.task( 'cachebust', [ 'scripts', 'styles' ], function() {
  return gulp.src( 'src/*.html' )
    .pipe( cachebust.references() )
    .pipe( gulp.dest( './' ) );
});
