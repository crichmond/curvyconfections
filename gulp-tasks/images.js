'use strict';

var gulp = require( 'gulp' );
var runSequence = require( 'run-sequence' );
var clean = require( 'gulp-clean' );

gulp.task( "images:clean", function() {
  return gulp.src( [
    'dist/images'
  ], {
      read: false
    })
    .pipe( clean( {
      force: true
    }) );
});

gulp.task( 'images', [ "images:clean" ], function() {
  return gulp.src( 'src/images/**/*' )
    .pipe( gulp.dest( 'dist/images/' ) );
});

