'use strict';

var gulp = require( 'gulp' );
var runSequence = require( 'run-sequence' );
var browserSync = require( 'browser-sync' ).create();


// BROWSER SYNC ------------------------------------------

gulp.task( 'serve', function() {
  browserSync.init( {
    server: {
      baseDir: "./"
    }
  });

  gulp.watch( [ "*.html", "dist/**/*" ] ).on( 'change', browserSync.reload );
});