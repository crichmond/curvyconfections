'use strict';

var gulp = require( 'gulp' ),
  watch = require( "gulp-watch" );


gulp.task( "watch:images", function() {
  gulp.watch( [
    "src/images/**/*"
  ], [ "images" ] );
});

gulp.task( "watch:styles", function() {
  gulp.watch( [
    "src/styles/**/*.scss"
  ], [ "styles" ] );
});

gulp.task( "watch:scripts", function() {
  gulp.watch( [
    "src/scripts/**/*.js"
  ], [ "scripts" ] );
});

gulp.task( "watch:statics", function() {
  gulp.watch( [
    "src/*.html",
  ], [ "copy:html" ] );
});

gulp.task( "watch", [ "watch:images", "watch:styles", "watch:scripts", "watch:statics" ] );