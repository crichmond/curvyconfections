# Allow crawling of all content
User-agent: *
Disallow: /src/
Disallow: /dist/
Disallow: /gulp-tasks/
Disallow: 404.html
Disallow: *.js
Disallow: bower.json
Disallow: package.json
Disallow: .htaccess
Disallow: .gitignore
Disallow: gulpfile.js