'use strict';

var gulp = require( 'gulp' ),
  runSequence = require( "run-sequence" );

require( 'require-dir' )( 'gulp-tasks' );

// don't cachebust for local dev
gulp.task( "default", [ "styles", "images", "scripts", "copy:html" ], function( cb ) {
  runSequence( 'watch', 'serve', cb );
});

// the cachebust task runs css/js builds and copies .html from src to root
gulp.task( "deploy", [ "images", "cachebust" ] );